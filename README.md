mystran Installator
===================

This role installs MYSTRAN (https://github.com/dr-bill-c/MYSTRAN) from scratch. CMAKE is compiled from sources.

Role Variables
--------------

variables are set in defaults/main.yaml:

* MYSTRAN_APT_PREREQS: APT installable packages
* MYSTRAN_APT_PREREQS_Ubuntu: ubuntu Specific packages
* MYSTRAN_CMAKE_GIT_URL: GIT url for CMAKE
* MYSTRAN_CMAKE_GIT_SRC_DIR: CMAKE GIT local folder
* MYSTRAN_GIT_URL: MYSTRAN GIT url to pull from
* MYSTRAN_GIT_SRC_DIR: MYSTRAN GIT local folder
* MYSTRAN_BINARY_TARGET: MYSTRAN binary local target
* MYSTRAN_GIT_DISCARD_MODIFIED: if "yes", modified files from local repo are discarded


Example Playbook
----------------


    - hosts: laptops
      become: true
      roles:
	 - { role: mystran, tags: ["fea", "cad"] }

License
-------

MIT

Author Information
------------------

Nicolas Cordier (numenic)
